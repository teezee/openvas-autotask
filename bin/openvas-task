#!/bin/bash
###
# Copyright (c) 2018-2020 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# openvas-autotask
# Using a configuration file, this script can create and delete
# targets, alerts, schedules, and tasks for openvas.
# It uses the OpenVAS CLI 'omp'.
###
# TODO: scheduling on kali openvas seems broken. switch to cronjobs and at

# error handling
#set -eu # -o pipefail
#shopt -s failglob inherit_errexit

# usage
usage() {
	echo "usage: $(basename "$0") [-c | -d <file>]";
	echo "options:";
	echo -e "\t-c <file>: create openvas task and dependencies"
	echo -e "\t-d <file>: delete openvas task and dependencies"
	echo -e "\t   <file>: path of the task config file";
	echo -e "\t-u <user>: OMP username (default: admin)"
	echo -e "\t-w <pass>: OMP password"
	exit 0;
}

# global conf with OpenVAS admin user and password (beware special chars).
LIB="$(dirname ${BASH_SOURCE[0]%/*})/../lib"
ETC="$(dirname ${BASH_SOURCE[0]%/*})/../etc"
CONF="${ETC}/openvas-task.conf"

# check omp command
command -v omp >/dev/null 2>&1 || { echo >&2 "ERROR: omp required. Please install 'openvas-cli'"; usage; exit 1; }

# load libs
source "${LIB}"/openvas-config.shlib # provides parsing and sanitizing the config files


# get port_lists
select_port_list_id() {
	local DEFAULT=$(omp_xml '<get_port_lists filter="OpenVAS AND Default"/>' | grep "port_list id" | grep -oP '(?<=").*?(?=")')
	local ALL_IANA=$(omp_xml '<get_port_lists filter="IANA AND TCP AND UDP"/>' | grep "port_list id" | grep -oP '(?<=").*?(?=")')
	PORTLIST_ID="${ALL_IANA}"
	#printf "portlist_id: %s\n" "${PORTLIST_ID}" >&2
}


select_config_id() {
	local default=$(omp_cmd -g | grep -e "fast ultimate$" | cut -f1 -d' ')
	case "${CONFIG}" in
		FAST) CONFIG_ID=$(omp_cmd -g | grep -e "fast$" | cut -f1 -d' ');;
		FULT) CONFIG_ID=$(omp_cmd -g | grep -e "fast ultimate$" | cut -f1 -d' ');;
		DEEP) CONFIG_ID=$(omp_cmd -g | grep -e "deep$" | cut -f1 -d' ');;
		DULT) CONFIG_ID=$(omp_cmd -g | grep -e "deep ultimate$" | cut -f1 -d' ');;
		*) CONFIG_ID="${default}";;
	esac
	#printf "config_id: %s\n" "${CONFIG_ID}" >&2
}

# read the following vars from config_file
read_config() {
	if [ ! -f "${1}" ]; then
		echo "ERROR: File ${1} not found.";
		usage; exit 1
	fi

	sanitize_config "${1}"

	# parse schedule date, time, and period
	parse_date "${DATE}";
	parse_time "${TIME}";
	parse_period "${PERIOD}";
	#print_config
}


create() {
	# we could also use xml files, but that has escape issues
	# omp --xml "$(< [filename].xml)"
	local config_id="${CONFIG_ID}"

	# create target
	local target_id=$(omp_cmd -T | grep "${NAME}-target" | cut -d' ' -f1)
	if [ -z "${target_id}" ]; then
		out=$(omp_xml '<create_target>
			<name>'"${NAME}-target"'</name>
			<comment>'"${COMMENT}"'</comment>
			<hosts>'"${TARGETS}"'</hosts>
			<port_list id="'"${PORTLIST_ID}"'"/>
			</create_target>')
		echo "${out}"
		target_id=$(echo "${out}" | grep -oP '(?<=id=")([^"]+)')
	else printf "Target exists already id=%s\n" "${target_id}"
	fi

	# create alert
	# <method> has to be sent on 1line
	local alert_id=$(omp_xml '<get_alerts filter="'"name=${EMAIL}"'"/>' | grep "alert id" | grep -oP '(?<=").*?(?=")')
	if [ -z "${alert_id}" ]; then
		out=$(omp_xml '<create_alert>
		    <name>'${EMAIL}'</name>
		    <comment>'${COMMENT}'</comment>
		    <condition>Always</condition>
		    <event>Task run status changed<data><name>status</name>Done</data></event>
		    <method>Email<data><name>to_address</name>'${EMAIL}'</data><data><name>from_address</name>OpenVAS@uni-konstanz.de</data><data><name>subject</name>[OpenVAS-Manager] Task &apos;\$n&apos;: \$e</data><data><name>notice</name>2</data><data><name>notice_attach_format</name>c402cc3e-b531-11e1-9163-406186ea4fc5</data></method>
			</create_alert>')
		echo "${out}"
		alert_id=$(echo "${out}" | grep -oP '(?<=id=")([^"]+)')
	else printf "Alert exists already id=%s\n" "${alert_id}"
	fi

	# create schedule
	local schedule_id=$(omp_xml '<get_schedules filter="'"name=${NAME}-schedule"'"/>' | grep "schedule id" | grep -oP '(?<=").*?(?=")')
	if [ -z "${schedule_id}" ]; then
		out=$(omp_xml '<create_schedule>
            <name>'${NAME}-schedule'</name>
            <comment>'${COMMENT}'</comment>
            <first_time>
                    <day_of_month>'${DAY}'</day_of_month>
                    <month>'${MONTH}'</month>
                    <year>'${YEAR}'</year>
                    <hour>'${HOUR}'</hour>
                    <minute>'${MIN}'</minute>
            </first_time>
            <period>'${FREQ}'<unit>'${UNIT}'</unit></period>
			</create_schedule>')
		echo "${out}"
		schedule_id=$(echo "${out}" | grep -oP '(?<=id=")([^"]+)')
	else printf "Schedule already exists id=%s\n" "${schedule_id}"
	fi

	# create task
	local tasks=$(omp_xml '<get_tasks filter="'"name=${NAME}-task"'" details="'"0"'" />' | grep -i "task id" | grep -oP '(?<=").*?(?=")')
	if [ -z "${tasks}" ]; then
		out=$(omp_xml '<create_task>
            <name>'"${NAME}-task"'</name>
            <comment>'"${COMMENT}"'</comment>
            <config id="'"${config_id}"'"/>
            <target id="'"${target_id}"'"/>
            <schedule id="'"${schedule_id}"'" />
            <alert id="'"${alert_id}"'" />
			</create_task>')
		echo "${out}"
		tasks=$(echo "${out}" | grep -oP '(?<=id=")([^"]+)')
	else printf "Tasks already exists ids=%s" "${tasks}"
	fi
}

delete() {
	# first delete all tasks due to dependencies (can have collisions)
	local tasks=$(omp_xml '<get_tasks filter="'"name=${NAME}-task"'" details="'"0"'" />' | grep -i "task id" | grep -oP '(?<=").*?(?=")')
	for t in $tasks; do
		local out=$(omp_xml '<delete_task task_id="'"${t}"'" ultimate="'"1"'" />')
		echo "${out}"
	done

	local schedule_id=$(omp_xml '<get_schedules filter="'"name=${NAME}-schedule"'"/>' | grep "schedule id" | grep -oP '(?<=").*?(?=")')
	out=$(omp_xml '<delete_schedule schedule_id="'"${schedule_id}"'"/>')
	echo "${out}"

	local target_id=$(omp_cmd -T | grep "${NAME}-target" | cut -d' ' -f1)
	out=$(omp_xml '<delete_target target_id="'"${target_id}"'"/>')
	echo "${out}"

	local alert_id=$(omp_xml '<get_alerts filter="'"name=${EMAIL}"'"/>' | grep "alert id" | grep -oP '(?<=").*?(?=")')
	out=$(omp_xml '<delete_alert alert_id="'"${alert_id}"'"/>')
	echo "${out}"
}

# set defaults & check arguments
CONFIG_FILE=""
USER="admin"
PASS=""
HOST="localhost"
PORT="9390"

while getopts ":hc:d:u:w:" opt; do
	case "${opt}" in
		c)
			FUNC=create;
			CONFIG_FILE="${OPTARG}";
			;;
		d)
			FUNC=delete;
			CONFIG_FILE="${OPTARG}";
			;;
		u)
			USER="${OPTARG}";
			;;
		w)
			PASS="${OPTARG}";
			;;
		h)
			usage;
			;;
		*)
			usage;
			;;
	esac
done
shift $((OPTIND-1))

# check for config file argument
if [ -z "$CONFIG_FILE" ]; then
	echo >&2 "ERROR: Missing argument <file>.";
	usage; exit 1;
fi

# check password argument or conf
if [ -z "${PASS}" ]; then
	echo >&2 "WARN: no omp password provided, trying config ..."
 	if [ ! -f "$CONF" ]; then
		echo >&2 "ERROR: $CONF not found.";
		echo "printf \"USER=admin\nPASS=[admin_password]\" >> $CONF";
		usage; exit 1
	fi
	echo >&2 "INFO: loading config ${CONF}"
	. "${CONF}"
fi

omp_cmd() {
	cmd="omp -u ${USER} -w \"${PASS}\" -h ${HOST} -p ${PORT} $@"
	#printf "DEBUG %s\n" "${cmd}" >&2
	eval ${cmd} 2>&1
}

omp_xml() {
	omp_cmd "-i -X '$@'"
}

# start main
read_config $CONFIG_FILE;
select_port_list_id; 
select_config_id;
$FUNC;
