#!/usr/bin/env bash
###
# Copyright (c) 2020 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# openvas-inotify
# watches the config directory for CLOSE events and automatically reads/sanitizes configs and adds them to openvas.
###

set -euf -o pipefail

command -v inotifywait >/dev/null 2>&1 || { echo >&2 "ERROR: inotifywait required (inotify-tools)."; usage; exit 1; }

BIN="$(dirname ${BASH_SOURCE[0]%/*})/bin"
WATCH="./inconf"
OUT="./outconf"

mkdir -p "${WATCH}" "${OUT}"

inotifywait --monitor -r -e close_write -e moved_to --format '%e %w %f' ${WATCH} |
while read -r event watch file; do
	(
	echo "${event} in ${watch} on file ${file}"
	${BIN}/sanitize-config "${watch}/${file}" > "${OUT}/${file}"
	"${BIN}"/openvas-task -c "${OUT}/${file}"
	rm "${watch}/${file}"
	)  &
done

