###
# Copyright (c) 2020 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# readconfig.shlib
# functions to safely read config files
#
# sources:
# - https://tritarget.org/static/Bash%2520script%2520configuration%2520file%2520format.html
# - https://unix.stackexchange.com/questions/175648/use-config-file-for-my-shell-script
#
# example usage:
# 	source readconfig.shlib
#   myvar="$(config_get configfile.conf varname 'default-value')"
###



##
# read the config file
# $1 file
# $2 variable
config_read_file() {
	(grep -E "^${2}=" -m 1 "${1}" 2>/dev/null || echo "VAR=__UNDEF__") \
    	| head -n 1 | cut -d '=' -f 2-
}

##
# get variable from config
# $1 file
# $2 variable
# $3 default value
config_get() {
	local val
	val="$(config_read_file "${1}" "${2}")"
	if [ "${val}" = "__UNDEF__" ]; then
		val="${3}"
	fi
	printf -- "%s" "${val}"
}

